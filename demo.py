from time import process_time_ns as ns
from ctypes import sizeof

from sllist.sllist import SLList
from dllist.dllist import DLList


def demo():
    
    uplist = []
    for i in range(100):
        uplist.append(i)
    
    print("Singly-Linked List Demo")

    list1 = SLList()
    
    time1 = ns()
    for i in range(len(uplist)):
        list1.append(uplist[i])
    time2 = ns()
    total = time2 - time1
    print("Time taken to fill List 1: {x} ns".format(x=total))

    print("List 1 length: " + str(len(list1)))
    print("List 1 items:")
    time1 = ns()
    for i in list1.items():
        print(i)
    time2 = ns()
    total = time2-time1
    print("Time taken to print List1 items: {x} ns".format(x=total))
    
    time1 = ns()
    list2 = SLList(list1)
    time2 = ns()
    total = time2 - time1
    print("Time taken to fill List 2 with List 1: {x} ns".format(x=total))
    
    print("List 2 items:")
    time1 = ns()
    for i in list2.items():
        print(i)
    time2 = ns()
    total = time2 - time1
    print("Time taken to print List 2 items: {x} ns".format(x=total))

    list3 = SLList()
    
    time1 = ns()
    for i in range(len(list1)):
        list3.push(list1[i])
        list3.append(list2[i])
    time2 = ns()
    total = time2 - time1
    print(
        "Time taken to fill List 3 with items form lists 1 and 2: {x} ns".format(
            x=total))
    
    time1 = ns()
    if 99 in list3:
        time2 = ns()
        print("List 3 contains!")
        total = time2 - time1
        print("Time taken to determine whether List 3 contains: {x} ns".format(
            x=total))
    else:
        time2 = ns()
        print("List 3 doesn't contain.")
        total = time2 - time1
        print("Time taken to determine whether List 3 contains: {x} ns".format(
            x=total))
    
    print("List 3 items:")
    time1 = ns()
    for node in list3:
        print(node.obj)
    time2 = ns()
    total = time2 - time1
    print("Time taken to print List 3 items: {x} ns".format(x=total))
    
    time1 = ns()
    for i in range(len(list3)):
        if i % 2:
            list1.push(list3[i])
        else:
            list2.append(list3[i])
    time2 = ns()
    total = time2 - time1
    print("Time take to divde List 3 between 1 and 2: {x} ns".format(x=total))    
    print("List1: | List2:")
    for i in range(len(list1)):
        print(str(list1[i]) + " | " + str(list2[i]))

    print("Doubly-Linked List Demo")

    list1 = DLList()
    
    time1 = ns()
    for i in range(len(uplist)):
        list1.append(uplist[i])
    time2 = ns()
    total = time2 - time1
    print("Time taken to fill List 1: {x} ns".format(x=total))

    print("List1 length: " + str(len(list1)))
    print("List 1 items:")
    time1 = ns()
    for i in list1.items():
        print(i)
    time2 = ns()
    total = time2 - time1
    print("Time taken to print List 1 items: {x} ns".format(x=total))
    
    time1 = ns()
    list2 = DLList(list1)
    time2 = ns()
    total = time2 - time1
    print("Time taken to fill List2 with List 1 items: {x} ns".format(x=total))

    print("List 2 items:")
    time1 = ns()
    for i in list2.items():
        print(i)
    time2 = ns()
    total = time2 - time1
    print("Time taken to print List 2 items: {x} ns".format(x=total))

    list3 = DLList()
    
    time1 = ns()
    for i in range(len(list1)):
        list3.append(list1[i])
        list3.prepend(list2[i])
    time2 = ns()
    total = time2 - time1
    print("Time taken to fill list 3 with List 1 and 2 items: {x} ns".format(
                x=total))
    
    time1 = ns()
    if 99 in list3:
        time2 = ns()
        print("List 3 contains!")
    else:
        time2 = ns()
        print("List3 doesn't contain.")
    print(
        "Time taken to determine whether or not List 3 contains: {x} ns".format(
                x=total))

    print("List 3 items:")
    time1 = ns()
    for node in list3:
        print(node)
    time2 = ns()
    total = time2 - time1
    print("Time taken to print List 3: {x} ns".format(x=total))
    
    time1 = ns()
    for i in range(len(list3)):
        if i % 2:
            list1.insert(len(list1) - 1, list3[i])
        else:
            list2.insert(i, list3[i])
    time2 = ns()
    total = time2 - time1
    print("Time taken to divide List 3 between 1 and 2: {x} ns".format(x=total))

    print("List1: | List2:")
    for i in range(len(list1)):
        print(str(list1[i]) + " | " + str(list2[i]))

from sys import exit

if __name__ == "__main__":
    demo()
    exit(0)
