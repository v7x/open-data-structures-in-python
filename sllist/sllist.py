from collections.abc import MutableSequence

class SLList(MutableSequence):

    head = None
    tail = None
    length = 0

    class _node:

        obj = None
        next = None

        def __init__(self, obj):
            self.obj = obj
            self.next = None

    def __init__(self, iterable=None):
        if iterable is not None:
            for i in range(len(iterable)):
                self.append(iterable[i])

    def __len__(self):
        return self.length

    def __iter__(self):
        iterable = []
        i = 0
        node = self.head
        while node is not None:
            iterable.append(node)
            node = node.next
            i += 1
        return iter(iterable)

    def __setitem__(self, key, item):
        if key > len(self):
            raise IndexError("Index {k} not in SLList.".format(k=key))
        newnode = self._node(item)
        sliter = iter(self)
        slist = []
        for i in sliter:
            slist.append(i)
        slist[key] = newnode

        self.head = None
        self.length = 0
        for node in slist:
            self.append(node.obj)

    def append(self, item):
        newnode = self._node(item)
        if self.head is None:
            self.head = newnode
            self.tail = self.head
        else:
            self.tail.next = newnode
            self.tail = newnode

        self.length += 1

    def __getitem__(self, key):
        if key == len(self) and self.tail is not None:
            return self.tail.obj
        if self.head is not None:
            node = self.head
            for i in range(key):
                if node.next is None:
                    raise IndexError(
                        "Index {x} out of range {r}.".format(x=i, r=key)
                    )
                else:
                    node = node.next

            return node.obj
        else:
            raise IndexError("Cannot get item from empty list.")

    def push(self, item):
        newnode = self._node(item)
        if self.head is None:
            self.head = newnode
            self.tail = newnode
            self.length += 1
        else:
            newnode.next = self.head
            self.head = newnode
            self.length += 1

    def pop(self):
        popnode = self.head
        self.head = self.head.next
        return popnode.obj

    def items(self):
        slist = iter(self)
        itemlist = []

        for node in slist:
            itemlist.append(node.obj)

        return itemlist

    def __contains__(self, item):
        return item in self.items()

    def __delitem__(self, key):
        if key > len(self):
            raise IndexError("Index {x} not in SLList.".format(x=key))

        deliter = iter(self)
        dellist = []
        for i in deliter:
            dellist.append(i)
        del dellist[key]

        self.length = 0
        for node in dellist:
            self.append(node.obj)

    def insert(self):
        raise NotImplementedError(
            "Method insert() not implemented for class SLList."
        )
