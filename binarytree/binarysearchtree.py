#!/usr/bin/python

from collections.abc import MutableMapping

NODEREPR = "Node containing.keyect {o}. Depth={d}, height={h}, size={s}, parent={p}, left={l}, right={r}"

class BinarySearchTree(MutableMapping):

    root = None
    total = 0

    def __init__(self):
        self.nil = self.Node(None, None)
        self.nil.left = self.nil.right = self.nil.parent = self.nil
        if n is not None:
            self.root = self.nil
            self.root.left = self.root.right = self.root.parent = self.nil
            self.total += 1
        else:
            self.root = self.nil

    class Node:

        left = None
        right = None
        parent = None

        def __init__(self, key, data, parent=None):
            self.key = key
            if parent is not None:
                self.parent = parent

        def __str__(self):
            return str(self.key)

        def __repr__(self):
            return NODEREPR.format(o=self.key, d=self.depth(), h=self.height(),
                    s=self.size(), p=str(self.parent), l=str(self.left),
                    r=str(self.right))

        def depth(self):
            d = 0
            node = self
            while node.parent.key is not None:
                node = node.parent
                d += 1
            return d

        def size(self):
            s = 1
            if self.left.key is not None:
                s += self.left.size()
            if self.right.key is not None:
                s += self.right.size()
            return s

        def height(self):
            h = 1
            if self.left.key is not None and self.right.key is not None:
                h +=  max(self.left.height(), self.right.height())
            elif self.left.key is None and self.right.key is not None:
                h += self.right.height()
            elif self.left.key is not None and self.right.key is None:
                h += self.left.height()
            return h

        def recurse(self):
            if self.left.key is not None and self.right.key is not None:
                self.left.recurse()
                self.right.recurse()
            elif self.left.key is None and self.right.key is not None:
                self.right.recurse()
            elif self.left.key is not None and self.right.key is None:
                self.left.recurse()

        def add_child(self, cnode):
            if cnode.key < self.key:
                self.left = cnode
                self.left.parent = self
                return True
            elif cnode.key > self.key:
                self.right = cnode
                self.right.parent = self
                return True
            else:
                return False

        def gather(self, gathered=[]):
            gathered.append(self.key)
            if self.left.key is not None:
                gathered = self.left.gather(gathered)
            if self.right.key is not None:
                gathered = self.right.gather(gathered)
            return gathered

    def __len__(self):
        if self.root is not self.nil:
            return self.root.size()
        else:
            return 0

    def __contains__(self, key):
        node = self.root
        while node is not self.nil:
            if key < node.key:
                node = node.left
            elif key > node.key:
                node = node.right
            else:
                return True
        return False

    def __getitem__(self, key):
        ret = self.find_node(key)
        return ret.data

    def __setitem__(self, key, data):
        self.add(key, data)

    def __delitem__(self, key):
        self.discard(key)

    def traverse(self):
        node = self.root
        prev = self.nil
        next = self.nil
        while node is not self.nil:
            if prev is node.parent:
                if node.left is not self.nil:
                    next = node.left
                elif node.right is not self.nil:
                    next = node.right
                else:
                    next = node.parent
            elif prev is node.left:
                if node.right is not self.nil:
                    next = node.right
                else:
                    next = node.parent
            else:
                next = node.parent
            prev = node
            node = next

    def size(self):
        node = self.root
        prev = self.nil
        next = self.nil
        n = 0

        while node is not self.nil:
            if prev is node.parent:
                n += 1
                if node.left is not self.nil:
                    next = node.left
                elif node.right is not self.nil:
                    next = node.right
                else:
                    next = node.parent

            elif prev is node.left:
                if node.right is not self.nil:
                    next = node.right
                else:
                    next = node.parent
            else:
                next = node.parent
            prev = node
            node = next
        return n
                    
    def find_node(self, key):
        node = self.root
        retnode = self.nil
        while node is not self.nil:
            if key < node.key:
                retnode = node
                node = node.left
            elif key > node.key:
                node = node.right
            else:
                return node
        if retnode is self.nil:
            return self.nil
        return retnode

    def find(self, key):
        node = self.find_node(key)
        if node is self.nil:
            return None
        return node.key

    def add(self, key, data):
        pnode = self.find_last(key)
        newnode = self.Node(key, data)
        newnode.left = newnode.right = newnode.parent = self.nil
        res = False
        if self.root is self.nil:
            self.root = newnode
            res = True
        else:
            res = pnode.add_child(newnode)

        if res:
            self.total += 1
        return res

    def add_node(self, node):
        pnode = self.find_last(node.key)
        if node.left is None:
            node.left = self.nil
        if node.right is None:
            node.right = self.nil
        node.parent = self.nil
        res = False
        if self.root is self.nil:
            self.root = node
            res = True
        else:
            res = pnode.add_child(node)

        if res:
            self.total += 1
        return res

    def find_last(self, key):
        if key is None:
            return self.nil
        node = self.root
        prev = self.nil
        while node is not self.nil:
            prev = node
            if (key < node.key):
                node = node.left
            elif (key > node.key):
                node = node.right
            else:
                return node
        return prev

    def splice(self, node):
        next = self.nil
        p = self.nil

        if node.left is not self.nil:
            next = node.left
        else:
            next = node.right
        if node is self.root:
            next.parent = self.nil
            self.root = next
        else:
            p = node.parent
            if p.left is node:
                p.left = next
            else:
                p.right = next
        if next is not self.nil:
            next.parent = p

        self.total -= 1

    def discard(self, key):
        node = self.find_node(key)
        if node is not self.nil:
            if node.left is self.nil or node.right is self.nil:
                self.splice(node)
            else:
                w = node.right
                while w.left is not self.nil:
                    w = w.left
                node.key = w.key
                self.splice(w)
            return True
        else:
            return False

    def __iter__(self):
        return iter(self.root.gather())

    def rotate_left(self, node):
        rotor = node.right
        rotor.parent = node.parent

        if rotor.parent != self.nil:
            if rotor.parent.left == node:
                rotor.parent.left = rotor
            else:
                rotor.parent.right = rotor
        node.right = rotor.left
        if node.right != self.nil:
            node.right.parent = node

        node.parent = rotor
        rotor.left = node
        if node == self.root:
            self.root = rotor
            self.root.parent = self.nil

    def rotate_right(self, node):
        rotor = node.left
        rotor.parent = node.parent

        if rotor.parent != self.nil:
            if rotor.parent.left == node:
                rotor.parent.left = rotor
            else:
                rotor.parent.right = rotor

        node.left = rotor.right
        if node.left != self.nil:
            node.left.parent = node

        node.parent = rotor
        rotor.right = node
        if node == self.root:
            self.root = rotor
            self.root.parent = self.nil

    def clear(self):
        self.__init__()
