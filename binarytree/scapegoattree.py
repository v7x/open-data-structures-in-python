#!/usr/bin/python

from math import log

from binarysearchtree import BinarySearchTree

class ScapegoatTree(BinarySearchTree):
    
    total = 0
    bound = 0

    def __init__(self):
        super(BinarySearchTree, self).__init__()
        self.nil = self.Node(None, None)
        self.nil.left = self.nil.right = self.nil.parent = self.nil
        self.root = self.nil

    class Node(BinarySearchTree.Node):

        def __init__(self, key, data, parent=None):
            super(ScapegoatTree.Node, self).__init__(key, data, parent)

        def size(self):
            s = 1
            if self.left.key is not None:
                s += self.left.size()
            if self.right.key is not None:
                s += self.right.size()
            return s

    def rebuild(self, node):
        kids = node.size()
        p = node.parent
        a = [self.nil for i in range(kids)]
        self._array_pack(node, a, 0)
        #have to check if p is None because parents arent set to self.nil
        #somewhere, not sure where.
        if p is self.nil or p is None:
            self.root = self.build_balanced(a, 0, kids)
            self.root.parent = self.nil
        elif p.right is node:
            p.right = self.build_balanced(a, 0, kids)
            p.right.parent = p
        else:
            p.left = self.build_balanced(a, 0, kids)
            p.left.parent = p

    def _array_pack(self, node, a, i):
        if node is self.nil:
            return i
        i = self._array_pack(node.left, a, i)
        a[i] = node
        i += 1
        return self._array_pack(node.right, a, i)

    def build_balanced(self, a, i, nodnum):
        if nodnum == 0:
            return self.nil
        m = nodnum // 2
        index = i + m
        a[index].left = self.build_balanced(a,i,m)
        if a[index].left is not self.nil:
            a[index].left.parent = a[index]
        a[index].right = self.build_balanced(a, index + 1, nodnum - m - 1)
        if a[index].right is not self.nil:
            a[index].right.parent = a[index]
        return a[index]

    def add(self, key, data):
        (node, d) = self.add_with_depth(key, data)
        if d > log(self.bound, 1.5):
            check = node.parent
            if check.parent is not self.nil:
                while 3 * check.size() <= 2 * check.parent.size():
                    check = check.parent
                self.rebuild(check.parent)
        return d >= 0

    def add_with_depth(self, key, data=None):
        node = key
        if not isinstance(node, self.Node):
            node = self.Node(key, data)

        node.left = node.right = node.parent = self.nil

        if self.root is self.nil:
            self.root = node
            self.total += 1
            self.bound += 1
            return (node, 0)
        
        done = False
        d = 0
        check = self.root
        while not done:
            if key < check.key:
                if check.left is self.nil:
                    check.left = node
                    node.parent = check
                    done = True
                else:
                    check = check.left
            elif key > check.key:
                if check.right is self.nil:
                    check.right = node
                    node.parent = check
                    done = True
                else:
                    check = check.right
            else:
                return (self.nil, -1)

            d += 1

        self.total += 1
        self.bound += 1
        return (node, d)

    def discard(self, key):
        if super().discard(key):
            if 2 * self.total < self.bound:
                self.rebuild(self.root)
                self.bound = self.total
            return True
        return False
