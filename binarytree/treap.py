#!/usr/bin/python

#from collections.abc import MutableSet
from random import randint
from sys import maxsize

from binarysearchtree import BinarySearchTree

class Treap(BinarySearchTree):

    priorities = []

    def __init__(self):
        super(BinarySearchTree, self).__init__()
        self.nil = self.Node(None)
        self.nil.left = self.nil.right = self.nil.parent = self.nil
        self.root = self.nil

    class Node(BinarySearchTree.Node):

        priority = randint(0, maxsize)

        def __init__(self, key, data, parent=None):
            super(Treap.Node, self).__init__(key, data, parent)

    def rotate_left(self, node):
        rotor = node.right
        rotor.parent = node.parent

        if rotor.parent is not self.nil:
            if rotor.parent.left is node:
                rotor.parent.left = rotor
            else:
                rotor.parent.right = rotor

        node.right = rotor.left
        if node.right is not self.nil:
            node.right.parent = node

        node.parent = rotor
        rotor.left = node
        if node is self.root:
            self.root = rotor
            self.root.parent = self.nil

    def rotate_right(self, node):
        rotor = node.left
        rotor.parent = node.parent
        
        if rotor.parent is not self.nil:
            if rotor.parent.left is node:
                rotor.parent.left = rotor
            else:
                rotor.parent.right = rotor

        node.left = rotor.right
        if node.left is not self.nil:
            node.left.parent = node

        node.parent = rotor
        rotor.right = node
        if node is self.root:
            self.root = rotor
            self.root.parent = self.nil

    def add(self, key, data):
        newnode = self.Node(key, data, self.nil)
        while newnode.priority in self.priorities:
            newnode = self.Node(key, self.nil)

        newnode.left = newnode.right = self.nil

        pnode = self.find_last(key)
        if pnode is not self.nil:
            res = pnode.add_child(newnode)
        else:
            self.root = newnode
            res = True
        
        if res:
            self.bubble_up(newnode)
            self.total += 1

    def bubble_up(self, node):
        while self.root is not node and node.parent.priority > node.priority:
            if node.parent.right is node:
                self.rotate_left(node.parent)
            else:
                self.rotate_right(node.parent)

        if node.parent is self.nil:
            self.root = node

    def discard(self, key):
        node = self.find_last(key)
        if node is not None and node.key == key:
            self.trickle_down(node)
            self.splice(node)
            if node.priority in self.priorities:
                self.priorities.remove(node.priority)
            return True
        return False

    def trickle_down(self, node):
        while node.left is not self.nil or node.right is not self.nil:
            if node.left is self.nil:
                self.rotate_left(node)
            elif node.right is self.nil:
                self.rotate_right(node)
            elif node.left.priority < node.right.priority:
                self.rotate_right(node)
            else:
                self.rotate_left(node)
            if self.root is node:
                self.root = node.parent
