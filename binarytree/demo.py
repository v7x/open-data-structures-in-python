#!/usr/bin/python

from random import choice, randint, seed
from time import process_time_ns as ns
#from sys import maxsize

from binarysearchtree import BinarySearchTree
from treap import Treap
from scapegoattree import ScapegoatTree
from redblacktree import RedBlackTree

def demo(datasize):
    seed(44)
    data = []
    print("Generating {d} random numbers.".format(d=datasize))
    for i in range(datasize):
        newint = randint(0, datasize * 10)
        data.append(newint)
    print("Numbers generated.")
    print(data)
    
    #tree1 = BinarySearchTree()
    #tree2 = Treap()
    #tree3 = ScapegoatTree()
    tree4 = RedBlackTree()
    trees = [tree4]#[tree1, tree2, tree3, tree4]
    
    counter = 0

    for tree in trees:
        if counter == 0:
            print("Binary Search Tree demo.")
        elif counter == 1:
            print("Treap demo.")
        elif counter == 2:
            print("Scapegoat Tree demo.")
        elif counter == 3:
            print("Red-Black Tree demo.")

        print("Filling tree with data")
        total = 0
        for i in range(len(data) - 1):
            time1 = ns()
            tree.add(data[i])
            time2 = ns()
            addtime = time2 - time1
            print("Time taken to add {d} to tree: {t} ns".format(
                    d=data[i], t=addtime))
            total += addtime

        for i in tree:
            print(repr(tree.find_last(i)))

        avgtime = total / len(data)
        print("Average time to add data to tree: {a} ns".format(a=avgtime))

        time1 = ns()
        nodecount = len(tree)
        time2 = ns()
        total = time2 - time1
        print("Number of nodes in tree: {n}".format(n=nodecount))
        print("Time to count nodes in tree: {t} ns".format(t=total))

        time1 = ns()
        height = tree.root.height()
        time2 = ns()
        total = time2 - time1
        print("Height of root node: {h}".format(h=height))
        print("Time to find height of root: {t} ns".format(t=total))

        time1 = ns()
        tree.traverse()
        time2 = ns()
        total = time2 - time1
        print("Time to traverse tree: {t} ns".format(t=total))

        time1 = ns()
        tree.root.recurse()
        time2 = ns()
        total = time2 - time1
        print("Time to recursively traverse tree: {t} ns".format(t=total))
    
        indecies = range(len(data) - 1)
        found = 0
        foundtime = 0
        notfound = 0
        notfoundtime = 0

        for i in range(25):
            index = choice(indecies)
            time1 = ns()
            if data[index] in tree:
                time2 = ns()
                total = time2-time1
                print("Found {d} in tree in {t} ns.".format(d=data[index],
                            t=total))
                foundtime += total
                found += 1
            else:
                time2 = ns()
                total = time2 - time1
                print("Searched tree for {t} ns but didn't find {d}.".format(
                            t=total, d=data[index]))
                notfoundtime += total
                notfound += 1

        if notfound > 0:
            avgtime = notfoundtime / notfound
            print("Items not found: {x}".format(x=notfound))
            print("Average time looking for missing items: {x} ns".format(
                        x=avgtime))
        if found > 0:
            avgtime = foundtime / found
            print("Items found: {x}".format(x=found))
            print("Average time looking for items: {x} ns".format(x=avgtime))

        deltime = 0
        dellist = []
        delnum = 1
        for i in tree:
            time1 = ns()
            res = tree.discard(i)
            time2 = ns()
            total = time2 - time1
            print("Time to discard {i} from tree: {t} ns".format(i=i, t=total))
            print(delnum)
            dellist.append(i)
            deltime += total
            delnum += 1 

        dellen = len(dellist)
        avgtime = deltime / dellen
        print("Average time to discard items from tree: {t} ns".format(t=avgtime))
        print("Items in tree: {i}".format(i=len(tree)))
        for i in tree:
            print(i)

        tree.clear()
        
        counter += 1
 
from sys import exit

if __name__ == "__main__":
    datasizes = [100, 1000, 10000]
    for i in datasizes:
        demo(i)
    exit()
