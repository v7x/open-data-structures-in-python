#!/usr/bin/python

from random import randint, choice
from time import process_time_ns as ns
from sys import maxsize

def demo(datasize):
    data = []
    print("Generating {i} random numbers.")
    for i in range(datasize):
        data.append(randint(0, datasize * 10))
    print("Numbers generated.")

    return 0

from sys import exit

if __name__ == "__main__":
    datasizes = [100, 1000, 10000]
    for i in datasizes:
        demo(i)
    exit(0)

