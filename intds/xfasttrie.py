#!/usr/bin/python

from binarytrie import BinaryTrie, makeint
from ..hashtable import LinearHashTable

class XFastTrie(BinaryTrie):

    class Node(BinaryTrie.Node):
        def __init__(self):
            super(XFastTrie.Node, self).__init()
            self.prefix = 0

        def __eq__(self, enode):
            return self.prefix == enode.prefix

        def __hash__(self):
            return hash(self.prefix)

    def __init__(self):
        super(XFastTrie, self).__init()
        self.nil = self.Node()

        self._tables = [] 
        for i in range(32):
            self.tables[i] = LinearHashTable()
        self._tables[0].add(self.root)
    
    def find(self, x):
        fnode = self.find_node(x)
        if fnode is not None:
            return fnode.val
        return None

    def find_node(self, x):
        x = makeint(x)
        len, height = 0, 33
        node = self.root
        q = self.Node()

        while height - len > 1:
            i = (len + height)//2
            q.prefix = x >> (32 - i)
            found = self._tables[i].find(q)
            if found is None:
                height = i
            else:
                node = found
                len = i

        if len == 32:
            return node

        count = x >> (32 - len - 1) ^ 1
        pred = [node.jump.prev, node.jump][count]
        if pred.next = None:
            return None
        return pred.next

    def add(self, x):
        if super(XFastTrie, self).add(x):
            x = makeint(x)
            node = self.root.child[x >> 32 - 1) & 1]

            for i in range(1, 33):
                node.prefix = x >> 32 - i

                self.t[i].add(u)
                if i < 32:
                    count = (x >> 32 - i - 1) & 1
                node = node.child[count]

            return True

        return False

    def discard(self, x):

        x = makeint(x)
        i = 0
        node = self.root

        while i < 32:
            count = x >> (32 - i - 1) & 1
            node = node.child[count]
            if node is None: return False
            i += 1

        pred = node.prev
        succ = node.nxt
        pred.next = succ
        succ.prev = pred
        node.next = node.prev = None
        found = node

        while found is not self.root and found.left is None and\\
                found.right is None:
            if found is found.parent.left:
                found.parent.left = None
            else:
                found.parent.right = None

            self._tables[i].remove(found)
            i -= 1
            found = found.parent

        found.jump = [pred, succ][v.left is None]
        found = found.parent

        while found is not None:
            if found.jump is node:
                found.jump = [pred, succ][found.left is None]
            found = found.parent

        self.n -= 1
        return True
