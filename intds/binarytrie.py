#!/usr/bin/python

def makeint(x):
    if x is not None and not isinstance(x, int):
        x = int(x)
    return x

from collections.abc import Set

class BinaryTrie(Set):
    
    class Node:

        def __init__(self, val=None, left=None, right=None, jump=None,
                parent=None):
            self.val = makeint(val)
            self.parent = parent
            self.child = [left, right]
            self.jump = jump

        @property
        def left(self):
            return self.child[0]

        @left.setter
        def left(self, x):
            self.child[0] = x

        @property
        def right(self):
            return self.child[1]

        @right.setter
        def right(self, x):
            self.child[1] = x

        @property
        def prev(self):
            return self.child[0]

        @prev.setter
        def prev(self, x):
            self.child[0] = x

        @property
        def next(self):
            return self.child[1]

        @next.setter
        def next(self, x):
            self.child[1] = x

        def __int__(self):
            return self.val

    def __init__(self):
        self.dummy = self.Node()
        self.dummy.prev = self.dummy.next = self.dummy
        self.root = self.Node()
        self.root.jump = self.dummy
        self.count = 0

    def __len__(self):
        return self.count

    def find(self, x):
        x = makeint(x)
        node = self.root
        i = 0
        kid = None

        while i < 32:
            kid = (x >> 32 - i - 1) & 1
            if node.child[kid] is None: break
            node = node.child[kid]
            i += 1

        if i == 32:
            return node.val

        node = [node.jump, node.jump.next][kid]
        if node is self.dummy: return None
        return node.val

    def __contains__(self, x):
        if x is not None:
            if makeint(x) == self.find(x):
                return True
        return False

    def add(self, x):
        x = makeint(x)
        node = self.root

        #1 - search for x until we fall from tree
        i = 0
        kid = None

        while i < 32:
            kid = (x >> 32 - i - 1) & 1
            if node.child[kid] is None: break
            node = node.child[kid]
            i += 1

        if i == 32: return False

        pred = [node.jump.prev, node.jump][kid]
        node.jump = None

        #2 - add path to x

        while i < 32:
            kid = (x >> 32 - i - 1) & 1
            node.child[kid] = Node()
            node.child[kid].parent = node
            node = node.child[kid]
            i += 1
        node.val = x

        #3 - add node to linked list
        node.prev = pred
        node.next = pred.next
        node.prev.next = node
        node.next.prev = node

        #4 - walk back up to update jump points
        walk = node.parent
        while walk is not None:
            if (walk.left is None and (walk.jump is None or 
                        makeint(walk.jump.val) > x))\
                or (walk.right is None and (walk.jump is None or
                            makeint(walk.jump.val) < x)):
                    walk.jump = node
            walk = walk.parent

        self.count += 1
        return True

    def discard(self, x):
        x = makeint(x)
        node = self.root

        #1 - find leaf containing x
        i = 0
        kid = None

        while i < 32:
            kid = (x >> 32 - i - 1) & 1
            if node.child[kid] is None: return False
            node = node.child[kid]
            i += 1

        #2 - remove node from linked list
        node.prev.next = node.next
        node.next.prev = node.prev
        walk = node

        #3 - delete nodes on path to node
        for i in range(31, 0, -1):
            kid = (x >> 32 - i - 1) & 1
            walk = walk.parent
            walk.child[kid] = None
            if walk.child[1-kid] is not None: break

        #4 - update jump points
        pred = node.prev
        succ = node.next
        walk.jump = [pred,succ][walk.left is None]
        walk = walk.parent

        while walk is not None:
            if walk.jump is node:
                walk.jump = [pred,succ][walk.left is None]
            walk = walk.parent

        self.count -= 1
        return True
