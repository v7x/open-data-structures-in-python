#!/usr/bin/python

from random import randint, choice, seed
from time import process_time_ns as ns
from sys import maxsize

from sort import mergesort, quicksort, heapsort, countsort, radixsort

def demo(datasize):
    #seed(1)
    nums = []
    print("Generating {size} random numbers.".format(size=datasize))
    for i in range(datasize):
        nums.append(randint(0, datasize * 10))
    print("Numbers generated.")

    #print("Original list:\n{a}".format(a=nums))

    data = []
    for i in nums:
        data.append(i)
        
    print("Performing merge sort.")
    time1 = ns()
    sorted = mergesort(data)
    time2 = ns()
    total = time2 - time1
    #print("Merge-sorted list:\n{a}".format(a=sorted))
    print("Time taken to merge-sort list: {t} ns".format(t=total))

    data.clear()
    for i in nums:
        data.append(i)

    print("Performing quick sort.")
    time1 = ns()
    sorted = quicksort(data)
    time2 = ns()
    total = time2 - time1
    #print("Quick-sorted list:\n{a}".format(a=sorted))
    print("Time taken to quick-sort list: {t} ns".format(t=total))

    data.clear()
    for i in nums:
        data.append(i)

    print("Performing heap sort.")
    time1 = ns()
    sorted = heapsort(data)
    time2 = ns()
    total = time2 - time1
    #print("Heap-sorted list:\n{a}".format(a=sorted))
    print("Time taken to heap-sort list: {t} ns".format(t=total))

    data.clear()
    for i in nums:
        data.append(i)

    print("Performing count sort.")
    time1 = ns()
    sorted = countsort(data)
    time2 = ns()
    total = time2 - time1
    #print("Count-sorted list:\n{a}".format(a=sorted))
    print("Time taken to count-sort list: {t} ns".format(t=total))

    data.clear()
    for i in nums:
        data.append(i)

    print("Performing radix sort.")
    time1 = ns()
    sorted = radixsort(data)
    time2 = ns()
    total = time2 - time1
    #print(Radix-sorted list:\n{a}".format(a=sorted))
    print("Time taken to radix-sort list: {t} ns".format(t=total))

    print("{i} number test completed.".format(i=datasize))
    return 0

from sys import exit

if __name__ == "__main__":
    datasizes = [100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000]
    for i in datasizes:
        demo(i)
    exit(0)

