Implemenation of the lessons taught by [Open Data Structures](https://opendatastructures.org)
in a manner consistant with Python's built-in data types.

Note: Chapter 2, Array-Based Lists, is skipped by due to the ease of implementation.
