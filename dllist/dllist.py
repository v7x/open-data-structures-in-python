from collections.abc import MutableSequence

class DLList(MutableSequence):

    class _node:
        
        obj = None
        prev = None
        next = None

        def __init__(self, obj):
            self.obj = obj
            self.next = None
            self.prev = None

    _dummy = None
    length = 0

    def __init__(self, iterable=None):
        self._dummy = self._node(None)
        self._dummy.next = self._dummy
        self._dummy.prev = self._dummy
        if iterable is not None:
            for i in iterable:
                self.append(i)
    
    def __len__(self):
        return self.length

    def __iter__(self):
        iterable = []
        i = 0
        node = self._dummy.next
        while node is not self._dummy:
            iterable.append(node.obj)
            node = node.next
            i += 1
        return iter(iterable)

    def __setitem__(self, key, item):
        self._getnode(key).obj = item

    def prepend(self, item):
        newnode = self._node(item)
        newnode.prev = self._dummy
        newnode.next = self._dummy.next
        self._dummy.next.prev = newnode
        self._dummy.next = newnode
        self.length += 1

    def append(self, item):
        newnode = self._node(item)
        newnode.prev = self._dummy.prev
        newnode.next = self._dummy
        self._dummy.prev.next = newnode
        self._dummy.prev = newnode
        self.length += 1

    def _getnode(self, key):
        if key > len(self):
            raise IndexError("Index {x} not in DLList".format(x=key)) 
        
        if key >= 0:
            if key < len(self) / 2:
                node = self._dummy.next
                for i in range(key):
                    node = node.next
                return node
            else:
                node = self._dummy
                for i in range(len(self) - key):
                    node = node.prev
                return node

        else:
            if -key < len(self) / 2:
                node = self._dummy
                for i in range(-key):
                    node = node.prev
                return node
            else:
                node = self._dummy.next
                for i in range(len(self) + key):
                    node = node.next
                return node

    def __getitem__(self, key):
        return self._getnode(key).obj

    def insert(self, key, item):
        oldnode = self._getnode(key)
        newnode = self._node(item)

        oldnode.prev.next = newnode
        newnode.prev = oldnode.prev
        oldnode.prev = newnode
        newnode.next = oldnode

        self.length += 1

    def items(self):
        itemlist = []

        for node in iter(self):
            itemlist.append(node)

        return itemlist

    def __contains__(self, item):
        return item in self.items()

    def __delitem__(self, key):
        delnode = self[key]

        delnode.next.prev = delnode.prev
        delnode.prev.next = delnode.next

        del delnode
        
        self.length -= 1
