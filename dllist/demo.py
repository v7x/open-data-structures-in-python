﻿from dllist import DLList

def demo():
    print('Doubly-Linked List Demo')
    uplist = []
    for i in range(100):
        uplist.append(i)

    list1 = DLList()

    time1 = ns()
    for i in range(len(uplist)):
        list1.append(uplist[i])
    time2 = ns()
    total = time2 - time1
    print('Time taken to fill List 1: {x} ns'.format(x=total))

    print('List1 length: ' + str(len(list1)))
    print('List 1 items:')
    time1 = ns()
    for i in list1.items():
        print(i)
    time2 = ns()
    total = time2 - time1
    print('Time taken to print List 1 items: {x} ns'.format(x=total))

    time1 = ns()
    list2 = DLList(list1)
    time2 = ns()
    total = time2 - time1
    print('Time taken to fill List2 with List 1 items: {x} ns'.format(x=total))

    print('List 2 items:')
    time1 = ns()
    for i in list2.items():
        print(i)
    time2 = ns()
    total = time2 - time1
    print('Time taken to print List 2 items: {x} ns'.format(x=total))

    list3 = DLList()

    time1 = ns()
    for i in range(len(list1)):
        list3.append(list1[i])
        list3.prepend(list2[i])
    time2 = ns()
    total = time2 - time1
    print('Time taken to fill list 3 with List 1 and 2 items: {x} ns'.format(
        x=total
    ))

    time1 = ns()
    if 99 in list3:
        time2 = ns()
        print('List 3 contains!')
    else:
        time2 = ns()
        print("List3 doesn't contain.")
    print('Time taken to determine whether or not List 3 contains: {x} ns'.format(
        x=total
    ))

    print('List 3 items:')
    time1 = ns()
    for node in list3:
        print(node)
    time2 = ns()
    total = time2 - time1
    print('Time taken to print List 3: {x} ns'.format(x=total))

    time1 = ns()
    for i in range(len(list3)):
        if i % 2:
            list1.insert(len(list1) - 1, list3[i])
        else:
            list2.insert(i, list3[i])
    time2 = ns()
    total = time2 - time1
    print('Time taken to divide List 3 between 1 and 2: {x} ns'.format(x=total))

    print('List1: | List2:')
    for i in range(len(list1)):
        print(str(list1[i]) + ' | ' + str(list2[i]))
