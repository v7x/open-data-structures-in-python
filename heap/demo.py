#!/usr/bin/python

from random import randint, choice, seed
from time import process_time_ns as ns
from sys import maxsize

from binaryheap import BinaryHeap
from meldableheap import MeldableHeap

def demo(datasize):
    seed(1)
    data = []
    print("Generating {i} random numbers.")
    for i in range(datasize):
        data.append(randint(0, datasize * 10))
    print("Numbers generated.")

    heap1 = BinaryHeap()
    heap2 = MeldableHeap()
    heaps = [heap1, heap2]

    counter = 0

    for heap in heaps:
        if counter == 0:
            print("Binary Heap demo.")
        elif counter == 1:
            print("Meldable Heap demo.")

        print("Filling heap with data.")
        
        ttime = 0
        for i in data:
            time1 = ns()
            heap.add(i)
            time2 = ns()
            total = time2 - time1
            ttime += total
            print("Time taken to add {i} to heap: {t} ns".format(i=i, t=total))

        size = len(heap)

        print("Total time taken to add {n} items to heap: {t} ns".format(
                    n=size, t=ttime))
        print("Average time taken to add items to heap: {t} ns".format(
                    t=ttime/size))

        print("Popping items from heap.")
        
        ttime = 0
        for i in range(len(heap)):
            time1 = ns()
            item = heap.pop()
            time2 = ns()
            total = time2 - time1
            ttime += total
            print("Time taken to pop {i} from heap: {t} ns".format(i=item,
                        t=total))
        print("Total time taken to pop {n} items from heap: {t} ns".format(
                    n=size, t=ttime))
        print("Average time taken to pop items from heap: {t} ns".format(
                    t=ttime/size))

    return 0

from sys import exit

if __name__ == "__main__":
    datasizes = [100, 1000, 10000]
    res = 0
    for i in datasizes:
        res = demo(i)
    exit(res)
