#!/usr/bin/python

from collections.abc import MutableSet

class BinaryHeap(MutableSet):
    
    heap = [None]
    total = 0

    def __init__(self, iterable=None):
        if iterable is not None:
            for i in iterable:
                self.add(i)

    def _left(self, i):
        return 2 * i + 1

    def _right(self, i):
        return 2 * (i + 1)

    def _parent(self, i):
        return (i - 1) // 2

    def add(self, item):
        if len(self.heap) < self.total + 1:
            self.resize()

        self.heap[self.total] = item
        self.total += 1
        self._bubble_up(self.total - 1)
        return True

    def _bubble_up(self, i):
        p = self._parent(i)
        while i > 0 and self.heap[i] < self.heap[p]:
            self.heap[i], self.heap[p] = self.heap[p], self.heap[i]
            i = p
            p = self._parent(i)

    def pop(self):
        item = self.heap[0]
        self.heap[0] = self.heap[self.total - 1]
        self.total -= 1
        self._trickle_down(0)
        if 3 * self.total < len(self.heap):
            self.resize()
        return item

    def discard(self):
        raise NotImplementedError("Heaps do not imlement 'discard' method.")

    def _trickle_down(self, i):
        while i >= 0:
            j = -1
            ridx = self._right(i)
            if ridx < self.total and self.heap[ridx] < self.heap[i]:
                lidx = self._left(i)
                if self.heap[lidx] < self.heap[ridx]:
                    j = lidx
                else:
                    j = ridx
            else:
                lidx = self._left(i)
                if lidx  < self.total and self.heap[lidx] < self.heap[i]:
                    j = lidx
            if j >= 0:
                self.heap[j], self.heap[i] = self.heap[i], self.heap[j]
            i = j

    def resize(self):
        growth = [None for i in range(self.total)]
        self.heap += growth

    def __contains__(self, i):
        return i in self.heap

    def __iter__(self):
        return iter(self.heap)

    def __len__(self):
        return self.total
