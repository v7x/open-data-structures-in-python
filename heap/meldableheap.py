#!/usr/bin/python

from collections.abc import MutableSet
from random import getrandbits

class MeldableHeap(MutableSet):
    
    total = 0
    root = None

    def __init__(self, iterable=None):
        if iterable is not None:
            for i in iterable:
                self.add(i)

    class Node:

        left = None
        right = None
        parent = None

        def __init__(self, item, parent=None):
            self.obj = item
            if parent is not None:
                self.parent = parent

    def __len__(self):
        return self.total

    def __contains__(self, i):
        return i in self.heap

    def __iter__(self):
        return iter(self.heap)

    def merge(self, node1, node2):

        if node1 is None:
            return node2
        if node2 is None:
            return node1

        if node2.obj < node1.obj:
            (node1, node2) = (node2, node1)

        if getrandbits(1) == 0:
            node1.left = self.merge(node1.left, node2)
            node1.left.parent = node1
        else:
            node1.right = self.merge(node1.right, node2)
            node1.right.parent = node1
        return node1

    def add(self, item):
        node = item
        if not isinstance(node, self.Node):
            node = self.Node(item)

        self.root = self.merge(node, self.root)
        self.root.parent = None
        self.total += 1
        return True

    def pop(self):
        item = self.root.obj
        self.root = self.merge(self.root.left, self.root.right)
        if self.root is not None:
            self.root.parent = None
        self.total -= 1
        return item

    def discard(self):
        raise NotImplementedError("Heaps do not implement 'discard' method.")
