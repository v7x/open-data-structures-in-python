#!/usr/bin/python

def compare(x, y):

    if x < y:
        return -1
    elif x > y:
        return 1
    else:
        return 0

def merge(arr0, arr1, arr):

    i0 = i1 = 0
    for i in range(len(arr)):
        if i0 == len(arr0):
            arr[i] = arr1[i1]
            i1 += 1
        elif i1 == len(arr1):
            arr[i] = arr0[i0]
            i0 += 1
        elif arr0[i0] <= arr1[i1]:
            arr[i] = arr0[i0]
            i0 += 1
        else:
            arr[i] = arr1[i1]
            i1 += 1

def mergesort(arr):

    length = len(arr)

    if length <= 1:
        return arr

    half = length // 2

    arr0 = mergesort(arr[:half])
    arr1 = mergesort(arr[half:])

    merge(arr0, arr1, arr)

    return arr

from random import randrange

def _qsort(arr, i, size):
    if size <= 1:
        return
    
    pivot = arr[i + randrange(0, size)]
    idx1, idx2, idx3 = i-1, i, i+size

    while idx2 < idx3:
        if arr[idx2] < pivot:
            idx1 += 1
            arr[idx1], arr[idx2] = arr[idx2], arr[idx1]
            idx2 += 1

        elif arr[idx2] > pivot:
            idx3 -= 1
            arr[idx2], arr[idx3] = arr[idx3], arr[idx2]

        else:
            idx2 += 1

    _qsort(arr, i, idx1-i+1)
    _qsort(arr, idx3, size-(idx3-i))

def quicksort(arr):
    _qsort(arr, 0, len(arr))
    return arr

from heap.binaryheap import BinaryHeap

def heapsort(arr):

    heap = BinaryHeap()
    heap.heap = arr
    heap.total = len(arr)
    half = heap.total // 2

    for i in range(half-1, -1, -1):
        heap._trickle_down(i)

    while heap.total > 1:
        heap.total -= 1
        (heap.heap[heap.total], heap.heap[0]) = (heap.heap[0], 
                heap.heap[heap.total])

        heap._trickle_down(0)
    
    arr.reverse()
    return arr

def countsort(arr):
    topval = max(arr) + 1

    countarr = [0 for i in range(topval)]

    for i in range(0, len(arr)-1):
        countarr[arr[i]] += 1

    for i in range(topval - 1):
        countarr[i] += countarr[i] - 1

    retarr = [None for i in range(len(arr))]

    for i in range(len(arr) - 1, 0, -1):
        countarr[arr[i]] -= 1
        retarr[countarr[arr[i]]] = arr[i]

    return retarr

def radixsort(arr):
    intbits = 32
    radbits = 8
    pow = 1 << radbits
    retarr = None

    for p in range(intbits//radbits): #num of bits in int/num of bits radix divides into
        countarr = [0 for i in range(pow)]
        retarr = [None for i in range(len(arr))]

        for i in range(len(arr)):
            bits = (arr[i] >> radbits * p) & (pow - 1)
            countarr[bits] += 1

        for i in range(1, pow):
            countarr[i] += countarr[i-1]

        for i in range(len(arr) - 1, -1, -1):
            bits = (arr[i] >> radbits * p) & (pow - 1)
            countarr[bits] -= 1
            retarr[countarr[bits]] = arr[i]

        arr = retarr

    return retarr
