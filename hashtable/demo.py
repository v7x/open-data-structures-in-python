from string import ascii_letters, digits, punctuation
from random import choice, randint
from time import process_time_ns as ns
from sys import maxsize

from hashtable import ChainedHashTable, LinearHashTable

def demo(datasize):
    numdata = []
    print("Generating random data")
    for i in range(datasize):
        newint = randint(0, datasize * 10)
        numdata.append(newint)
    print("Data generated.")

    print("ChainedHashTable demo")
    table = ChainedHashTable()
    print("Table hash: {h}".format(h=table.object_hash()))
    
    print("Filling table with data.")
    for i in range(len(numdata) - 1):
        table.add(numdata[i])
    print("Table filled.")

    print("Number of entries in table: " + str(len(table)))
    print("Number of rows: " + str(len(table.table)))

    indecies = range(len(numdata) - 1)
    foundtime = 0
    notfoundtime = 0
    found = 0
    notfound = 0

    for i in range(25):
        index = choice(indecies)
        time1 = ns()
        if numdata[index] in table:
            time2 = ns()
            print("Found {d} in table in {t} ns.".format(d=numdata[index],
                        t=time2-time1))
            foundtime += time2 - time1
            found += 1
        else:
            time2 = ns()
            print("Searched table for {t} ns but didn't find {d}.".format(
                        t=time2-time1, d=numdata[index]))
            notfoundtime += time2 - time1
            notfound += 1

    if notfound > 0:
        avgtime = notfoundtime / notfound
        print("Items not found: {x}".format(x=notfound))
        print("Average time looking for missing items: {x} ns".format(
                    x=avgtime))

    if found > 0:
        avgtime = foundtime / found
        print("Items found: {x}".format(x=found))
        print("Average time looking for items: {x} ns".format(x=avgtime))

    for count in range(len(table.table)):
        row = table.table[count]
        if len(row) > 0:
            print("Row number {c}:".format(c=count))
            print(row)

    print("Current table hash: {h}".format(h=table.object_hash()))
     
    print("LinearHashTable demo")

    table = LinearHashTable()
    print("Current table hash: {h}".format(h=table.object_hash()))

    print("Filling table with data.")
    for i in range(len(numdata) - 1):
        table.add(numdata[i])
    print("Table filled.")

    print("Number of entries in table: " + str(len(table)))
    print("Number of rows: " + str(len(table.table)))

    indecies = range(len(numdata) - 1)
    foundtime = 0
    notfoundtime = 0
    found = 0
    notfound = 0

    for i in range(25):
        index = choice(indecies)
        time1 = ns()
        if numdata[index] in table:
            time2 = ns()
            print("Found {d} in table in {t} ns.".format(d=numdata[index],
                        t=time2-time1))
            foundtime += time2-time1
            found += 1
        else:
            time2 = ns()
            print("Searched table for {t} ns but didn't find {d}.".format(
                        t=time2-time1, d=numdata[index]))
            notfoundtime += time2 - time1
            notfound += 1

    if notfound > 0:
        avgtime = notfoundtime / notfound
        print("Items not found: {x}".format(x=notfound))
        print("Average time looking for missing items: {x} ns".format(
                    x=avgtime))

    if found > 0:
        avgtime = foundtime / found
        print("Items found: {x}".format(x=found))
        print("Average time looking for items: {x} ns".format(x=avgtime))

    for count in range(len(table.table)):
        i = table.table[count]
        if i is not None and i != table.delobj:
            print("Row number {c}: {x}".format(c=count, x=i))

    print("Current table hash: {h}".format(h=table.object_hash()))

from sys import exit

if __name__ == "__main__":
    datasizes = [100, 1000, 10000]
    for i in datasizes:
        demo(i)
    exit(0)
