from random import randrange, getrandbits
from sys import maxsize, byteorder
from collections.abc import MutableSet
from math import log2

def rand_odd():
    return randrange(1, maxsize, 2)

def hash_code(item, length, odd):
    x = ((odd * hash(item)) % (2 ** 32)) >> (32 - int(log2(length)))
    return x

def multi_hash_code(parts, bitsize):
    if len(parts) == 0:
        return 0
    randbits = []
    hashes = []
    for i in parts:
        hashes.append(hash(i))
        randbits.append(getrandbits(bitsize))
    dubrandbits = getrandbits(2*bitsize)
    
    code = 0
    for i in range(len(hashes)):
        code += randbits[i] * hashes[i]

    code *= dubrandbits
    code %= 2 ** (2 * bitsize)
    code >>= bitsize

    return code

class ChainedHashTable(MutableSet):
    table = []
    count = 0
    dimension = 0

    def __init__(self):
        self._grow(2)
        self.odd = rand_odd()

    def object_hash(self):
        return multi_hash_code(self, 32)

    def __len__(self):
        return self.count

    def _grow(self, size):
        size -= 1
        size |= size >> 1
        size |= size >> 2
        size |= size >> 4
        size |= size >> 8
        size |= size >> 16
        size += 1
        oldtable = self.table
        self.table = [[] for i in range(size)]
        for i in oldtable:
            for item in i:
                self.add(item)

    def thash(self, item):
        x = hash_code(item, len(self.table), self.odd)
        return x

    def add(self, item):
        index = self.thash(item)
        if index < len(self.table) and item in self.table[index]:
            return False

        if index >= len(self.table):
            self._grow(len(self.table))

        self.table[index].append(item)
        self.count += 1
        if self.count / len(self.table) > 1:
            self._grow(len(self.table) + 1)
        return True

    def discard(self, item):
        index = self.thash(item)

        for i in range(len(self.table[index])):
            x = self.table[index][i]
            if x == item or x is item:
                del self.table[index][i]
                self.count -= 1

    def __contains__(self, item):
        if item in self.table[self.thash(item)]:
            return True
        else:
            return False

    def __iter__(self):
        iterable = []
        for row in self.table:
            if len(row) > 0:
                for i in row:
                    iterable.append(i)

        return iter(iterable)

def gen_tab():
    tab = [[], [], [], []]
    for row in tab:
        for i in range(256):
            x = getrandbits(32)
            row.append(x)
    return tab

class LinearHashTable(MutableSet):
    
    class _delObj:
        def __init__(self):
            pass
    
    table = []
    count = 0
    entries = 0

    def __init__(self, dimension=1):
        self.d = 1
        self.tab = gen_tab()
        self.resize()
        self.delobj = self._delObj()

    def object_hash(self):
        return multi_hash_code(self, 32)

    def __len__(self):
        return self.count

    def thash(self, item):
        x = hash(item)
        hashnum = (self.tab[0][x&0xff] \
                ^ self.tab[1][(x>>8)&0xff] \
                ^ self.tab[2][(x>>16)&0xff] \
                ^ self.tab[3][(x>>24)&0xff]) >> (32 - self.d)
        return hashnum

    def __contains__(self, item):
        index = self.thash(item)
        while self.table[index] is not None:
            if self.table[index] != self.delobj and item == self.table[index]:
                return True
            index = (index + 1) % len(self.table)
        return False

    def add(self, item):
        if item in self.table:
            return False
        size = 2 * (self.entries + 1)
        if size > len(self.table):
            self.resize()

        index = self.thash(item)
        while self.table[index] is not None and \
                self.table[index] != self.delobj:
            index = (index + 1) % len(self.table)
        if self.table[index] is None:
            self.entries += 1
        self.count += 1
        self.table[index] = item
        return True

    def discard(self, item):
        index = thash(item)
        while self.table[index] is not None:
            delitem = self.table[index]
            if delitem is not self.delobj and item == delitem:
                self.table[index] = self.delobj
                self.count -= 1
                if 8 * self.count < len(self.table):
                    self.resize()
                return delitem
            index = (index + 1) % len(self.table)
        return None

    def resize(self):
        self.d = 1
        while 1 << self.d < 3 * self.count:
            self.d += 1
        oldtable = self.table
        self.table = [None for i in range(1<<self.d)]
        self.entries = self.count
        for item in oldtable:
            if item is not None and item != self.delobj:
                index = self.thash(item)
                while self.table[index] is not None:
                    index = (index + 1) % len(self.table)
                self.table[index] = item

    def __iter__(self):
        iterable = []
        for item in self.table:
            if item is not None and item != self.delobj:
                iterable.append(item)
        return iter(iterable)
