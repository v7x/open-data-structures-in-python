#!/usr/bin/python

def bfs(graph, start):
    seen = [False for i in range(len(graph))]

    q = list()
    q.append(start)

    while len(q) > 0:
        i = q.pop(0)
        for j in graph.out_edges(i):
            if seen[j] = false:
                q.append(j)
                seen[j] = True

WHITE = 1
GREY = 0
BLACK = -1

def dfs_recursive(graph, start):
    colors = [None for i in range(len(graph))]
    _dfs(graph, start, color)

def _dfs_recursive(graph, i, color):
    color[i] = GREY
    for j in graph.out_edges(i):
        if color[j] == WHITE:
            color[j] = GREY
            _dfs(graph, j, color)
    color[i] = BLACK

def dfs(graph, start):
    color = [None for i in range(len(graph))]
    stack = list()
    stack.push(start)

    while len(stack) > 0:
        i = stack.pop(0)
        if color[i] == WHITE:
            color[i] = GREY
            for j in graph.out_edges(i):
                stack.append(j)

from collections import OrderedDict

class AdjacencyMatrix(Object):

    def __init__(self, x, y):
        xaxis = OrderedDict()
        for i in range(x):
            xaxis.[i] = False
        
        self._graph = OrderedDict()
        for i in range(y):
            self._graph[i] = xaxis.copy()

    def __len__(self) -> int:
        return len(self._graph)

    def __getitem__(self, xy:tuple) -> bool:
        return self._graph[xy[1]][xy[0]]

    def __setitem__(self, xy:tuple, val:bool):
        self._graph[xy[1]][xy[0]] = val

    def in_edges(self, y):
        row = list()
        for i in self._graph[y].keys():
            if i:
                row.append((i, y))
        return row

    def out_edges(self, x):
        col = list()
        for i in self._graph.keys():
            if self._graph[i][x]:
                col.append((x, i))
        return col

    def add_edge(self, x, y):
        self[(x, y)] = True

    def remove_edge(self, x, y):
        self[(x, y)] = False

    def has_edge(self, x, y):
        return self[(x, y)]

class AdjacencyLists(Object):

    def __init__(self, n=0):
        self._graph = [[] for i in range(n)]

    def __len__(self):
        return len(self._graph)
    
    def __getitem__(self, xy:tuple):
        return self.graph[xy[1]][xy[0]]

    def __setitem__(self, xy:tuple, val):
        self._graph[xy[1]][xy[0]] = val

    def remove_edge(self, x, y):
        for i in range len(self._graph[y] - 1):
            if self._graph[i] == x:
                del self._graph[i]
                return

    def has_edge(self, x, y):
        for i in self._graph[y]:
            if self._graph[i] == x:
                return True
        return False

    def out_edges(self, y):
        return self._graph[y]

    def in_edges(self, x):
        ret = list()
        for y in range(len(self._graph) - 1):
            if self.has_edge(x, y):
                ret.append(y)
        return ret
